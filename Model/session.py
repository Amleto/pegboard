from PySide.QtCore import QObject
from PySide.QtCore import Signal

from .data import Data, DataSignals
from .sessionFeatures import Features, QueueJumpMonitor

class Queries:
  def __init__(self, data):
    self.data = data

  def queueName(self, playerName):
    return self.data._d.findPlayersQueue(playerName).Name

  def playersInQueue(self, queueName):
    raise NotImplementedError("playersInQueue")

class Queries_:
  def __init__(self, data):
    self.data = data

  def queue(self, queueName):
    return self.data._d.findQueue(queueName)



class Session(DataSignals):

  operationRejected = Signal(str)

  def __init__(self):
    super(Session, self).__init__()

    self.data = Data()
    self._isRecording = 0
    self._operationBatch = []

    self.data.autoConnect(self, lambda signalName : signalName + "InData")

    self.features = {}

    self.Queries = Queries(self.data)

    self._Queries = Queries_(self.data)

  def save(self, filename):
    self.data.save(filename)

  def load(self, filename):
    self.data.load(filename);
    
  def reset(self):
    self.data.reset()

  def _move(self, playerName, fromQname, toQname):
    if Features.isEnabled(Features.DisableQueueJumping):
      #to do - fix this
      poolSize = 2 # 2 for testing, this is more normally 6-8 ime
      monitor = QueueJumpMonitor(self.data._d.waiting, poolSize)
      if not monitor.isMoveAllowed(playerName):
        if self._isRecording:
          self._operationBatch.append(_Operation(self.operationRejected, "move operation {0} from {1} -> {2}".format(playerName, fromQname, toQname)))
        return
    
    if fromQname:
      self.data.removePlayer(playerName, fromQname)
    self.data.addPlayer(playerName, toQname)

  def moveMulti(self, playerNames, fromQname, toQname):
    if Features.isEnabled(Features.DisableQueueJumping):
      #to do - fix this
      poolSize = 2 # 2 for testing, this is more normally 6-8 ime
      monitor = QueueJumpMonitor(self.data._d.waiting, poolSize)
      if not monitor.isMoveAllowed(playerName):
        if self._isRecording:
          self._operationBatch.append(_Operation(self.operationRejected, "move operation {0} from {1} -> {2}".format(playerName, fromQname, toQname)))
        return
    
    destQ = self._Queries.queue(toQname)
    if len(destQ) + len(playerNames) > destQ.capacity:
      #raise RuntimeError("Trying to move too many players into '{0}'".format(toQname))
      self._reject("Trying to move too many players into '{0}'".format(toQname))
      return

    for name in playerNames:
      if fromQname:
        self.data.removePlayer(name, fromQname)
      self.data.addPlayer(name, toQname)

  def renameQueue(self, oldName, newName):
    msg = str("Cannot rename container '{0}' because a container with "
                "that name already exists").format(newName)
    if self._rejectIfQueueExists(newName, msg):
      return

    self.data.renameQueue(oldName, newName)

  def addUpNext(self, name):
    msg = str("Cannot add container '{0}' because a container with "
                "that name already exists").format(name)
    if self._rejectIfQueueExists(name, msg):
      return
    self.data.addUpNext(name)
  
  def addCourt(self, name):
    msg = str("Cannot add container '{0}' because a container with "
                "that name already exists").format(name)
    if self._rejectIfQueueExists(name, msg):
      return
    self.data.addCourt(name)


  #def _autoConnectToData(self, data):
  #  for attr in Data.__dict__:
  #      if isinstance(Data.__dict__[attr], Signal):        
  #        slot = self.__getattribute__(attr + 'InData')
  #        data.__getattribute__(attr).connect(slot)

  #############################################################################

  def dataResetInData(self):
    if self._isRecording:
      self._operationBatch.append(_Operation(self.dataReset))

  def playerCreatedInData(self, playerName, gender):
    if self._isRecording:
      self._operationBatch.append(_Operation(self.playerCreated, playerName, gender))

  def playerDeletedInData(self, playerName):
    if self._isRecording:
      self._operationBatch.append(_Operation(self.playerDeleted, playerName))

  def playerAddedInData(self, playerName, queueName):
    if self._isRecording:
      self._operationBatch.append(_Operation(self.playerAdded, playerName, queueName))

  def playerRemovedInData(self, playerName, queueName):
    if self._isRecording:
      self._operationBatch.append(_Operation(self.playerRemoved, playerName, queueName))

  ################## queue added in data ################################
  
  def courtAddedInData(self, queueName):
    if self._isRecording:
      self._operationBatch.append(_Operation(self.courtAdded, queueName))

  def upNextAddedInData(self, queueName):
    if self._isRecording:
      self._operationBatch.append(_Operation(self.upNextAdded, queueName))

  def waitingAddedInData(self, queueName):
    if self._isRecording:
      self._operationBatch.append(_Operation(self.waitingAdded, queueName))

  def unavailableAddedInData(self, queueName):
    if self._isRecording:
      self._operationBatch.append(_Operation(self.unavailableAdded, queueName))

  def clubAddedInData(self, queueName):
    print 'clubAddedInData'
    if self._isRecording:
      self._operationBatch.append(_Operation(self.clubAdded, queueName))
  
  ################## queue removed in data ################################  
  
  def courtRemovedInData(self, queueName):
    if self._isRecording:
      self._operationBatch.append(_Operation(self.courtRemoved, queueName))

  def upNextRemovedInData(self, queueName):
    if self._isRecording:
      self._operationBatch.append(_Operation(self.upNextRemoved, queueName))

  def waitingRemovedInData(self, queueName):
    if self._isRecording:
      self._operationBatch.append(_Operation(self.waitingRemoved, queueName))

  def unavailableRemovedInData(self, queueName):
    if self._isRecording:
      self._operationBatch.append(_Operation(self.unavailableRemoved, queueName))

  def clubRemovedInData(self, queueName):
    if self._isRecording:
      self._operationBatch.append(_Operation(self.clubRemoved, queueName))

  #########################################################################
  
  def queueRenamedInData(self, oldname, newname):
    if self._isRecording:
      self._operationBatch.append(_Operation(self.queueRenamed, oldname, newname))

  #########################################################################

  def operationRejectedInData(self, queueName):
    if self._isRecording:
      self._operationBatch.append(_Operation(self.clubRemoved, queueName))

  #########################################################################

  def startUpdate(self):
    self._isRecording += 1

  def commit(self):
    self._isRecording -= 1
    
    assert self._isRecording >= 0
    
    if not self._isRecording:
      for op in self._operationBatch:
        print 'emit {0}{1}'.format(str(op.Signal), op.Args)
        op.Signal.emit(*op.Args)
      self._operationBatch = []

  def _reject(self, msg):
    if self._isRecording:
      self._operationBatch.append(_Operation(self.operationRejected, msg))
    else:
      raise RuntimeError(msg)

  def _rejectIfQueueExists(self, queueName, msg = ""):
    """If the queue exists then calls _reject and returns True.
    Returns False otherwise."""
    destQ = self._Queries.queue(queueName)
    if destQ is not None:
      msg_ = msg if msg else \
      "Rejecting because container '{0}' already exists".format(queueName)
      self._reject(msg_)
      return True
    return False

class _Operation:
    def __init__(self, signal, *args):
      self.Signal = signal
      self.Args = args

class BatchOperation:
  def __init__(self, session):
    self._session = session
    
  def __enter__(self):
    self._session.startUpdate()
    
  def __exit__(self, type, value, traceback):
    self._session.commit()