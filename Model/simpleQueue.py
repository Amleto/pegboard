

class queue(object):
  def __init__(self, capacity = float('inf')):
    self._items = []
    self.capacity = capacity
    
  def items(self):
    return list(self._items)

  def index(self, item):
    return self._items.index(item)

  def clear(self):
    self._items = []

  def remove(self, item):
    print 'remove', item.Name
    self._items.remove(item)

  def tryPushbackMulti(self, items):
    if self.capacity < len(self._items) + len(items):
      return False
    
    for item in items:
      self._pushback(item)
    return True

  def tryPushfrontMulti(self, items):
    if self.capacity < len(self._items) + len(items):
      return False
    
    for item in items:
      self._pushfront(item)
    return True

  def pushbackMulti(self, items):
    if self.capacity < len(self._items) + len(items):
      raise ValueError("capacity would be exceed")
    
    for item in items:
      self._pushback(item)
    return True

  def pushfrontMulti(self, items):
    if self.capacity < len(self._items) + len(items):
      raise ValueError("capacity would be exceed")
    
    for item in items:
      self._pushfront(item)
    return True

  def pushfront(self, item):
    if self.capacity == len(self._items):
      raise ValueError("capacity would be exceed")
    
    self._pushfront(item)

  def pushback(self, item):
    print "pushback", item.Name
    if self.capacity == len(self._items):
      raise ValueError("capacity would be exceed")
    
    self._pushback(item)

  def reorder(self, items):
    if len(items) != len(self._items):
      raise ValueError("size mismatch")
    
    diffOrder = False
    myitemiter = iter(self._items)
    for item in items:
      if item not in self._items:
        raise ValueError("item '{0}' not in the queue".format(item))
      if item != myitemiter.next():
        diffOrder = True

    if not diffOrder:
      return False

    self._items = list(items)
    return True

  def _pushfront(self, item):
    self._items.insert(0, item)

  def _pushback(self, item):
    self._items.append(item)

  def __iter__(self):
    for item in self._items:
      yield item

  def __contains__(self, item):
    return item in self._items

  def __len__(self):
    return len(self._items)

#todo - unit tests

