

def find(iterable, pred):
  return next((x for x in iterable if pred(x)), None)