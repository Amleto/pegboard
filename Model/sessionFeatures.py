

class QueueJumpMonitor:
  def __init__(self, waiting, poolSize):
    self._poolSize = poolSize
    self._waiting = waiting

    self._allowedNames = []
    self._isPicking = False

  def startPicking(self):
    qItems = self._waiting.items()[:self._poolsize]
    self._allowedNames = [mbr.Name for mbr in qItems] 
  
  def isMoveAllowed(self, playerName):
    return self._isPicking and playerName in self._allowedNames

  def stopPicking(self):
    self._allowedNames = []


class Features:

  DisableQueueJumping = "DisableQueueJumping"

  _featureStatuses = {}

  @staticmethod
  def enable(featureName):
    Features._featureStatuses[featureName] = True

  @staticmethod
  def disable(featureName):
    Features._featureStatuses[featureName] = False

  @staticmethod
  def isEnabled(featureName):
    return Features._featureStatuses.get(featureName, False)
