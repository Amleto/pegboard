from Model.member import Member

class Club(object):
  def __init__(self, name):
    self.Name = name
    self.Members = {} # all member info
  
  def addMember(self, name, gender):
    if self.Members.has_key(name):
      raise ValueError("'{0}' is already in the club".format(name))
    
    newMember = Member(name, gender)
    self.Members[name] = newMember
    return newMember

  def removeMember(self, name):
    del self.Members[name]

  def getMember(self, name):
    return self.Members.get(name)

  def members(self):
    return self.Members.itervalues()
