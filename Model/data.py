from PySide.QtCore import QObject
from PySide.QtCore import Signal



from .waitingList import WaitingList
from .unavailableList import UnavailableList
from .club import Club
from .court import Court

from . import find

class DataSignals(QObject):
  playerCreated = Signal(str, str)
  playerDeleted = Signal(str)

  playerAdded = Signal(str, str)
  playerRemoved = Signal(str, str)

  courtAdded = Signal(str)
  upNextAdded = Signal(str)
  waitingAdded = Signal(str)
  unavailableAdded = Signal(str)
  clubAdded = Signal(str)

  courtRemoved = Signal(str)
  upNextRemoved = Signal(str)
  waitingRemoved = Signal(str)
  unavailableRemoved = Signal(str)
  clubRemoved = Signal(str)

  queueRenamed = Signal(str, str)

  dataReset = Signal()

  operationRejected = Signal(str)

  def __init__(self):
    super(DataSignals, self).__init__()

  def __iter__(self):
    for attr, val in DataSignals.__dict__.iteritems():
      if isinstance(val, Signal):
        yield self.__getattribute__(attr)

  def _signalAndNames(self):
    for attr, val in DataSignals.__dict__.iteritems():
      if isinstance(val, Signal):
        yield self.__getattribute__(attr), attr

  def autoConnect(self, target, mapping = lambda name : name):
    print 'auto connecting {0} to {1}'.format(self, target)
    for signal, name in self._signalAndNames():
      print 'connecting {0} -> {1}'.format(name, mapping(name))
      r = signal.connect(target.__getattribute__(mapping(name)))

class Data(DataSignals):
  def __init__(self):
    super(Data, self).__init__()
    self._d = _Data();

  def reset(self):
    self._d = _Data()
    self.dataReset.emit()

  def save(self, filename):
    
    if not _Persistence.save(self._d, filename):
      raise RuntimeError("unable to save data")

  def load(self, filename):

    loadedData = _Persistence.load(filename)

    if not loadedData:
      raise RuntimeError("unable to load data")

    self._restore(loadedData)
  
  def _restore(self, data):
    """Restore the state of this instance to match that of 'data'
    by invoking public interface calls to ensure signals are sent
    enabling external parties to also restore their state accordingly
    """
    if data.club:
      self.addClub(data.club.Name)
      for mbr in data.club.members():
        self.registerPlayer(mbr.Name, mbr.Gender)
    
      if not data.unavailable:
        raise ValueError("expected 'Unavailable' queue but was not available")

    if data.upNexts:
      for upnext in data.upNexts:
        self.addUpNext(upnext.Name)
    
    if data.courts:
      for court in data.courts:
        self.addCourt(court.Name)

    # todo - do we want to support restoring player 'queue' positions
    # ie save players on court, in waiting, up next etc.

  def addClub(self, name):
    print 'Data - addClub'
    if self._d.club:
      raise ValueError("Can't add club - one already exists")

    self._d.club = Club(name)
    self.clubAdded.emit(name)

    if self._d.unavailable:
      raise ValueError("internal state error - please file a bug report")

    self.addUnavailable("Unavailable")
    self.addWaiting("Waiting")

    for mbr in self._d.club.members():
      self.addPlayer(mbr.Name, self._d.unavailable.Name)

  def removeClub(self, name):
    if not self._d.club:
      raise ValueError("Can't remove club - none exist")

    self._d.club = None
    self.clubRemoved.emit(name)
    
  def addUnavailable(self, name):
    if self._d.unavailable:
      raise ValueError("Can't add an unavailable list - one already exists")

    self._d.unavailable = UnavailableList(name)
    self.unavailableAdded.emit(name)

    #if self._d.club:
    #  for mbr in self._d.club.members():
    #    self.addPlayer(mbr.Name, name)

  def removeUnavailable(self, name):
    if not self._d.unavailable:
      raise ValueError("Can't remove an unavailable list - none exist")

    self._d.unavailable = None
    self.unavailableRemoved.emit(name)

  def addWaiting(self, name):
    if self._d.waiting:
      raise ValueError("Can't add a waiting list - one already exists")

    self._d.waiting = WaitingList(name)
    self.waitingAdded.emit(name)

  def removeWaiting(self, name):
    if not self._d.waiting:
      raise ValueError("Can't remove waiting list - none exist")

    self._d.waiting = None
    self.waitingRemoved.emit(name)

  def addUpNext(self, courtName):
    if self._d.findQueueIn(courtName, self._d.upNexts) is not None:
      raise ValueError("Can't add up next - one with that name already exists")
    
    self._d.upNexts.append(Court(courtName))
    self.upNextAdded.emit(courtName)

  def removeUpNext(self, courtName):
    q = self._d.findQueueIn(courtName, self._d.upNexts)
    if q is None:
      raise ValueError("Can't remove court - none exist with that name")
    
    self._d.upNexts.remove(q)
    self.upNextRemoved.emit(courtName)

  def addCourt(self, courtName):
    if self._d.findQueueIn(courtName, self._d.courts) is not None:
      raise ValueError("Can't add court - one with that name already exists")
      
    self._d.courts.append(Court(courtName))
    self.courtAdded.emit(courtName)

  def removeCourt(self, courtName):
    q = self._d.findQueueIn(courtName, self._d.courts)
    if q is None:
      raise ValueError("Can't remove court - none exist with that name")
    
    self._d.courts.remove(q)
    self.courtRemoved.emit(courtName)

  def registerPlayer(self, playerName, gender):
    if not self._d.club:
      raise ValueError("Need to add a club before you can register a player")
    
    member = self._d.club.addMember(playerName, gender)
    self.playerCreated.emit(member.Name, member.Gender)
    self.addPlayer(member.Name, self._d.unavailable.Name)
    return member

  def unregisterPlayer(self, playerName):
    self._d.club.removeMember(playerName)
    self.playerDeleted.emit(member.Name, member.Gender)
    return member

  def renameQueue(self, oldName, newName):
    qWithOldName = self._d.findQueue(oldName)
    if qWithOldName is None:
      raise ValueError("Cannot find container {0} to rename".format(oldName))
    
    qWithNewName = self._d.findQueue(newName)
    if qWithNewName is not None:
      raise ValueError("Cannot rename - container already exists with name {0}".format(oldName))

    qWithOldName.Name = newName
    self.queueRenamed.emit(oldName, newName)

  ######################  player move ops ###################################
  def addPlayer(self, playerName, queueName):
    q = self._d.findQueue(queueName)
    member = self._d.club.getMember(playerName)
    q.pushback(member)
    self.playerAdded.emit(playerName, queueName)

  def removePlayer(self, playerName, queueName):
    q = self._d.findQueue(queueName)
    member = self._d.club.getMember(playerName)
    q.remove(member)
    self.playerRemoved.emit(playerName, queueName)

  def reorderQueue(self, queueName, playerNames):
    """playerNames must be a list of all names in the queue
    in the desired order"""

    q = self._d.findQueue(queueName)
    members = q.items()
    currentNames = [mbr.Name for mbr in members]

    for name in playerNames:
      if name not in currentNames:
        raise ValueError(
            """player '{0}' is not already in queue {1},
            therefore they cant be part of a 
            reorder operation""".format(name, queueName))
      
    for mbr in members:
      q.remove(mbr)
      self.playerRemoved.emit(mbr.Name, queueName)

    for name in playerNames:
      member = find(members, lambda mbr : mbr.Name == name)
      q.pushback(member)
      self.playerAdded.emit(member.Name, queueName)

  def moveQueueContents(self, fromQname, toQname):
    q1 = self._d.findQueue(fromQname);
    q2 = self._d.findQueue(toQname);
    
    q1items = q1.items()
    q1.clear()

    for mbr in q1items:
      self.playerRemoved.emit(mbr.Name, fromQname)

    if q2.tryPushbackMulti(q1items):
      for mbr in q1items:
        self.playerAdded.emit(mbr.Name, toQname)
    else:
      q1.pushbackMulti(q1items)
      for mbr in q1items:
        self.playerAdded.emit(mbr.Name, fromQname)
      raise ValueError("unable to perform move - capacity problem")

class _Data(object):
  """
  Plain old data - easy pickling!
  """
  def __init__(self):
    self.courts = []
    self.upNexts = []
    self.waiting = None
    self.unavailable = None
    self.club = None

  @staticmethod
  def save(filename):
    import pickle
    try:
      with open(str(filename), "wb") as savefile:
        pickle.dump(self, savefile)
    except Exception, ex:
      print "save failed."
      print ex
      return False
    return True

  def load(self, filename):
    import pickle
    loadedInstance = None
    try:
      with open(str(filename), "r") as loadfile:
        loadedInstance = pickle.load(loadfile)
    except Exception, ex:
      print "session load failed:"
      print ex
      return None
    return loadedInstance

  def _initFromCopy(self, _data):
    self.courts = _data.courts
    self.upNexts = _data.upNexts
    self.waiting = _data.waiting
    self.unavailable = _data.unavailable
    self.club = _data.club

  def findQueue(self, name):
    for q in self._xQueues():
      if q.Name == name:
        return q

  def findQueueIn(self, name, queues):
    for q in queues:
      if q.Name == name:
        return q

  def _xQueues(self):
    for crt in self.courts:
      yield crt
    for un in self.upNexts:
      yield un

    if self.waiting is not None:
      yield self.waiting
    
    if self.unavailable is not None:
      yield self.unavailable

  def findPlayersQueue(self, playerName):
    """Returns the queue that the player is in"""
    mbr = self.club.getMember(playerName)
    if not mbr:
      raise ValueError("playerName {0} not found".format(playerName))
    
    for q in self._xQueues():
      if mbr in q:
        return q

class _Persistence(object):
  
  @staticmethod
  def save(obj, filename):
    import pickle
    try:
      with open(str(filename), "wb") as savefile:
        pickle.dump(obj, savefile)
    except Exception, ex:
      import traceback
      traceback.print_exc()

      return False
    return True

  @staticmethod
  def load(filename):
    import pickle
    loaded = None
    try:
      with open(str(filename), "r") as loadfile:
        loaded = pickle.load(loadfile)
    except Exception, ex:
      import traceback
      traceback.print_exc()
      return None
    return loaded