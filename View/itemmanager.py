from PySide.QtCore import QObject, QPointF, Signal

from config import Config
from item import Item
from itemqueue import ItemQueue

class ItemManager(QObject):
  moveQueueContents = Signal(str, str)
  movePlayers = Signal(list, str, str)

  reorderQueue = Signal(str, list)

  def __init__(self, queries):
    super(ItemManager, self).__init__()

    self.offset = QPointF(Config.Spacing, Config.Spacing)
    self.reset()

    self.queries = queries

  def reset(self):
    # players
    self.items = {}

    # things that players are attached to
    self.queues = []

    self.waiting = None
    self.upnexts = []
    self.courts = []

  def addItem(self, item):
    """really just register item with mgr"""
    self.items[item.text] = item

  def addItemToQueue(self, name, qName):
    q = self._getQ(qName)
    q.addItem( self.items[name] )

  def removeItemFromQueue(self, name, qName):
    q = self._getQ(qName)
    q.removeItem(self.items[name])

  def addCourt(self, q):
    self._addQueue(q)
    self.courts.append(q)

  def addUpNext(self, q):
    self._addQueue(q)
    self.upnexts.append(q)

  def setWaiting(self, q):
    self._addQueue(q)
    self.waiting = q

  def renameQueue(self, oldname, newname):
    q = self._getQ(oldname)
    if q:
      q.rename(newname)

  def _addQueue(self, q):
    self.queues.append(q)
    q.signals.doubleClicked.connect(self.clearQueue)

  def itemGrabbed(self, item):
    items = item.scene().selectedItems()
    self._detachItems(item, items)
      
  def itemReleased(self, item):
    items = item.scene().selectedItems()
    self._attachItems(item, items)

  def itemMoved(self, item):
    items = item.scene().selectedItems()

    self._itemsMoved(items, item)
  
  def clearQueue(self, queue):
    if queue in self.courts:
      self._moveAllItems(queue, self.waiting)

    if queue in self.upnexts:
      openCourt = self._findOpenCourt();
      if openCourt:
        self._moveAllItems(queue, openCourt)

  def forceRefresh(self):
    for q in self.queues:
      q.refresh()

  def graphicsItemIsRenamable(self, item):
    return hasattr(item, 'rename')

  def _itemsMoved(self, items, keyItem):
    # possible new destinations:
    incompatibleObjs = [obj for obj in items if type(obj) is not Item]
    if len(incompatibleObjs):
      print 'info: ignoring item move because foreign object included: ', incompatibleObjs
      return

    collidingQs = self._getCollidingQs(keyItem)

    queues = set()
    queuesGenerator = (self._getItemQ(itm) for itm in items if self._getItemQ(itm))
    queues.update(queuesGenerator)

    itemsNotInQueues = [itm for itm in items if not self._getItemQ(itm)]

    if itemsNotInQueues:
      # Shouldn't happen - we have a 'free' player(s) not in any container.
      # However, we can still attempt to add the player(s)
      # into the destination container
      if not collidingQs:
        return
      if len(collidingQs):
        playerNames = [itm.text for itm in itemsNotInQueues]
        print 'move players gesture {0}: {1} -> {2}'.format(playerNames, None, collidingQs[0].label.text())
        self.movePlayers.emit(playerNames, "", collidingQs[0].label.text())

    itemsByQ = { q : [i for i in q.items() if i in items] for q in queues }

    if not collidingQs:
      # move things back to where they were
      for (q, _) in itemsByQ.iteritems():
        q.refresh()
      return

    for (q, itemsInQ) in itemsByQ.iteritems():
      if not q:
        #if len(collidingQs):
        #  #collidingQs[0].addItems(itemsInQ)
        #  # todo - emit items added to queue
        #  for itm in itemsInQ:
        #    self.removePlayer(itm.text, q.Name)
        #    self.addPlayer(itm.text, q.Name)
        pass
      elif q in collidingQs:
        # items moved but still in the same queue - find the new order based
        # on ypos of all items in queue
        itemsInOriginalOrder = q.items()
        itemsInNewOrder = sorted(itemsInOriginalOrder, key= lambda x : x.pos().y())

        namesInNewOrder = [itm.text for itm in itemsInNewOrder]
        self.reorderQueue.emit(q.label.text(), namesInNewOrder)
        pass
      elif len(collidingQs):
        playerNames = [itm.text for itm in itemsInQ]
        print 'move players gesture {0}: {1} -> {2}'.format(playerNames, q.label.text(), collidingQs[0].label.text())
        self.movePlayers.emit(playerNames, q.label.text(), collidingQs[0].label.text())

  def _attachItems(self, item, items):
    if item not in items:
      q = self._getItemQ(item)
      if q:
        q._attach(item)

    for itm in items:
      q = self._getItemQ(itm)
      if q:
        q._attach(itm)

  def _detachItems(self, item, items):
    if item not in items:
      q = self._getItemQ(item)
      if q:
        q._detach(item)

    for itm in items:
      q = self._getItemQ(itm)
      if q:
        q._detach(itm)

  def _moveAllItems(self, fromQueue, toQueue):
    self.moveQueueContents.emit(fromQueue.label.text(), toQueue.label.text())

  def _findOpenCourt(self):
    # todo convert to query on model
    for court in self.courts:
      if len( court.items() ) == 0:
        return court
    return None

  def _getQ(self, qName):
    for q in self.queues:
      if q.name == qName:
        return q
    return None

  def _getItemQ(self, item):
    # todo convert to query on model

    qname = self.queries.queueName(item.text)
    return self._getQ(qname)

  def _getCollidingQs(self, item):
    collides = [q for q in self.queues if q.collidesWithItem(item)]
    return collides

  #class OneTimeSignalSlotTask(object):
  #  def __init__(self, source, signal, func, *args):
      
  #    self.source = source
  #    self.signal = signal
  #    self.args = args
  #    self.func = func

  #    QtCore.QObject.connect(self.source, QtCore.SIGNAL(self.signal), self)

  #  def __call__(self):
  #    self.func(*self.args)
  #    QtCore.QObject.disconnect(self.source, QtCore.SIGNAL(self.signal), self)
