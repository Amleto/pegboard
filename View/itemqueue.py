from PySide import QtGui, QtCore

from config import Config

class ItemQueue(QtGui.QGraphicsRectItem):

  DefaultSizeInItemHeightUnits = 4
  OuterSpacingMultiplier = 6

  def __init__(self, itemSize, capacity, text, *args, **kwds):
    QtGui.QGraphicsRectItem.__init__(self, *args, **kwds)
    
    self.itemSize = itemSize

    self.capacity = capacity if capacity > 0 else float("inf")
    self.isDynamicSizing = self.capacity == float("inf")
    self._items = []
    self.timelines = {}

    self.signals = QtCore.QObject()
    self.signals.setObjectName("QueueSignalEmitter")

    self.label = QtGui.QGraphicsSimpleTextItem(self)
    self.rename(text)

    self.setBrush(QtGui.QColor(200,250,200))
    pen = QtGui.QPen(QtCore.Qt.SolidLine)
    pen.setWidth(3)
    self.setPen(pen)
    self.setFlags(QtGui.QGraphicsItem.ItemIsMovable)# | QtGui.QGraphicsItem.ItemIsSelectable)
    self.setGraphicsEffect(QtGui.QGraphicsDropShadowEffect())

    self.setZValue(1.0)

  def refresh(self):
    self._stackItems(self._items)

  def addItem(self, item):
    if item in self._items:
      raise ValueError()

    if len(self._items) + 1 > self.capacity:
      self._rejectItem(item)
      return False

    self._addItem(item)
    if self.isDynamicSizing:
      self._resizeToFitItems()
    self._stackItem(item)
    return True

  def addItems(self, items):
    if len(self._items) + len(items) > self.capacity:
      self._rejectItems(item)
      return False

    self._addItems(items)
    if self.isDynamicSizing:
      self._resizeToFitItems()
    self._stackItems(items)

  def removeItem(self, item):
    if item not in self._items:
      return False

    self._removeItem(item)
    self._removeItemGui()

  def removeItems(self, items):
    for item in items:
      self._removeItem(item)
    self._removeItemGui()
  
  def repositionItem(self, item):
    isMoved = self._repositionItem(item)
    if isMoved:
      self._stackItems(self._items)
    else:
      self._stackItem(item)

  def repositionItems(self, items):
    map(self.repositionItem, items)

  def contains(self, item):
    return item in self._items

  def rename(self, newName):
    self.name = newName

    font = QtGui.QFont("tahoma")
    font.setBold(True)

    metrics = QtGui.QFontMetrics(font)
    textWidth = metrics.width(self.name)
    textHeight = metrics.height()
    
    self.itemStartOffset = textHeight + 2 * Config.Spacing

    numItems = self.capacity if self.capacity != float('inf') else ItemQueue.DefaultSizeInItemHeightUnits
    
    largestWidth = self.itemSize.width() if self.itemSize.width() > textWidth else textWidth

    width = largestWidth + ItemQueue.OuterSpacingMultiplier * Config.Spacing
    height = self.itemStartOffset + numItems * (self.itemSize.height() + 2 * Config.Spacing)

    self.setRect(0, 0, width, height)

    self.label.setText(self.name)
    self.label.setFont(font)
    self.label.setPos((width - textWidth)/2, Config.Spacing)

  def items(self):
    return [itm for itm in self._items]

  def clear(self):
    """Remove all items"""
    for item in self.items():
      self._removeItem(item)
      self._rejectItem(item)

  def mousePressEvent(self, event):
    if self.scene():
      self.scene().clearSelection()

  def mouseDoubleClickEvent(self, event):
    self.signals.emit( QtCore.SIGNAL("doubleClicked"), self )

  def _attach(self, item):
    print 'attach {2} pos: {0} -> {1}'.format(self.pos(), item.pos() - self.pos(), item.text) 
    item.setParentItem(self)
    item.setPos(item.pos() - self.pos())

  def _detach(self, item):
    print 'detach {2} pos: {0} -> {1}'.format(self.pos(), item.pos() + self.pos(), item.text)
    item.setParentItem(None)
    item.setPos(item.pos() + self.pos())

  def _addItem(self, item):
    """Add a new item to the queue (data operation)"""
    self._items.append(item)
    self._attach(item)

  def _addItems(self, items):
    srted = sorted(items, key=lambda item : item.pos().y())
    map(self._addItem, srted)

  def _removeItem(self, item):
    self._items.remove(item)
    self._detach(item)
    
  def _removeItems(self, items):
    map(self._removeItem, items)

  def _removeItemGui(self):
    if self.isDynamicSizing:
      self._resizeToFitItems()
    self._stackItems(self._items)

  def _repositionItem(self, item):
    """Given an item that is already owned and has been moved,
    call this method to reorganise the ordering based on item's current pos
    
    Returns True if the index of item in the queue has been chaged, False otherwise.
    """
    assert (len(self._items))
    assert (item in self._items)

    if len(self._items) == 1:
      return False

    itemToInsertAbove = None
    for otherItem in self._items:
      if otherItem is item:
        continue
      if item.pos().y() < otherItem.pos().y():
        itemToInsertAbove = otherItem
        break

    currentIndex = self._items.index(item)
    newIndex = self._items.index(itemToInsertAbove) if itemToInsertAbove else len(self._items) - 1
    if currentIndex < newIndex:
      newIndex -= 1
    
    if currentIndex == newIndex:
      return False

    # todo change this signal itemIndexChanged
    self._items.remove(item)
    self._items.insert(newIndex, item)
    return True

  def _stackItem(self, item):
    """Stack a new item (gui op)"""
    idx = self._items.index(item)
    relpos = self._getRelativePos(idx)
    item.animateMoveTo(relpos)
  
  def _stackItems(self, items):
    map(self._stackItem, items)

  def _rejectItem(self, item):
    rect = self.boundingRect().adjusted(0,0,self.pos().x(), self.pos().y())
    exitPoint = QtCore.QPointF(rect.width() + Config.Spacing, rect.height() + Config.Spacing)
    item.animateMoveTo(exitPoint)

  def _rejectItems(self, items):
    map(self._rejectItem, items)

  def _getRelativePos(self, index):
    """Return the relative position of an item element at the given"""
    xcount = 0
    ycount = index
    
    x = xcount * (Config.ItemWidth + 2 * Config.Spacing) + ItemQueue.OuterSpacingMultiplier * Config.Spacing / 2
    y = ycount * (Config.ItemHeight + 2 * Config.Spacing) + self.itemStartOffset

    return QtCore.QPointF(x, y)
 
  def _resizeToFitItems(self):
    if len(self._items) < ItemQueue.DefaultSizeInItemHeightUnits:
      return

    relpos = self._getRelativePos(len(self._items) - 1)
    minHeightThatContainsItem = relpos.y() + Config.ItemHeight + 2 * Config.Spacing
    #if minHeightThatContainsItem > self.rect().height():
    self.prepareGeometryChange()
    self.setRect(0, 0, self.rect().width(), minHeightThatContainsItem)

  