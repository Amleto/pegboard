from PySide import QtGui, QtCore

from config import Config

class Item(QtGui.QGraphicsRectItem):
  Size = QtCore.QSizeF(Config.ItemWidth, Config.ItemHeight)

  def __init__(self, mgr, text, color, *args, **kwd):
    QtGui.QGraphicsRectItem.__init__(self, QtCore.QRectF(0, 0, Item.Size.width(), Item.Size.height()), *args, **kwd)
    
    self.mgr = mgr

    self.signals = QtCore.QObject()
    self.signals.setObjectName("ItemSignalEmitter")

    self.timeline = QtCore.QTimeLine(500)
    self.timeline.setEasingCurve(QtCore.QEasingCurve.OutQuad);
    self.timeline.setUpdateInterval(10)
    self.animation = QtGui.QGraphicsItemAnimation()
    self.animation.setItem(self)
    self.animation.setTimeLine(self.timeline)

    QtCore.QObject.connect(self.timeline, QtCore.SIGNAL("finished()"), self._onAnimationFinished)

    self.setZValue(2.0)

    self.state = "still"
    
    self.label = QtGui.QGraphicsSimpleTextItem(self)
    self._rename(text)
    
    self.setFlags(QtGui.QGraphicsItem.ItemIsMovable | QtGui.QGraphicsItem.ItemIsSelectable)
      
    gradient = QtGui.QLinearGradient(0, 0, 0, Config.ItemHeight);
    gradient.setColorAt(0, color);
    gradient.setColorAt(1, color.darker());
    
    self.setBrush(gradient)
    pen = QtGui.QPen(QtCore.Qt.SolidLine)
    pen.setWidth(2)
    self.setPen(pen)

    self.setGraphicsEffect(QtGui.QGraphicsDropShadowEffect())
    
  def _rename(self, newName):
    self.text = newName
    self.label.setText(newName)
    self.label.setPos(Config.Spacing, 0)

  def mousePressEvent(self, sceneMouseEvent):
      super(Item, self).mousePressEvent(sceneMouseEvent)
      if sceneMouseEvent.button() == 1:
        if self.state == "still":
          self.state = "premove"
      self.mgr.itemGrabbed(self)
      
  
  def mouseMoveEvent(self, sceneMouseEvent):
      super(Item, self).mouseMoveEvent(sceneMouseEvent)
      if self.state == "premove":
        self.state = "moving"

      #self.label.setText ( "{0}_{1}".format( self.text, str(self.zValue()) ))
  
  def mouseReleaseEvent(self, sceneMouseEvent):
      super(Item, self).mouseReleaseEvent(sceneMouseEvent)
      
      self.mgr.itemReleased(self)
      if self.state == "moving":
        self.mgr.itemMoved(self)
      elif self.state == "premove":
        pass
      self.state = "still"

  def animateMoveTo(self, pos):
    """Setup and start an animation from 'start' to 'end'"""
    if (self.timeline.state() == QtCore.QTimeLine.Running):
      print 'info: animation aborted for item', self.text
      self.timeline.stop()

    self.animation.setPosAt(0.0, self.pos())
    self.animation.setPosAt(1.0, pos)
    
    self.timeline.start()

  def _onAnimationFinished(self):
    self.signals.emit(QtCore.SIGNAL("animationFinished"))
