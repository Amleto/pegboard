from PySide.QtGui import QInputDialog, QPushButton, QHBoxLayout
from PySide.QtGui import QVBoxLayout, QFileDialog, QRadioButton, \
                         QLabel, QLineEdit, QDialog, QWidget

from PySide.QtCore import Signal

def execInputDialog(labelStr = None):
  """
  exec() an input dialog.
  
  'signal' will be emitted with the user input if there is
  non-empty input and the user didn't cancel
  """
  dialog = QInputDialog()
  dialog.setWindowTitle("Input")
  if labelStr:
    dialog.setLabelText(labelStr)

  if (dialog.exec_() and dialog.textValue()):
    return dialog.textValue()
  return None

def execInputDialogAndEmit(signal, labelStr = None):
  input = execInputDialog(labelStr)
  if input:
    signal.emit( input )


class ControlWidget(QWidget):

  addClubSignal = Signal(str)
  addPlayerSignal = Signal(str, str)
  
  addCourtSignal = Signal(str)
  addUpNextSignal = Signal(str)
  addWaitingSignal = Signal(str)
  addUnavailableSignal = Signal(str)

  saveSignal = Signal(str)
  loadSignal = Signal(str)

  def __init__(self, *args):
    super(ControlWidget, self).__init__(*args)

    addClubBtn = QPushButton("add club")
    addPlayerBtn = QPushButton("add player")
    
    addCourtBtn = QPushButton("add court")
    addUpNextBtn = QPushButton("add up next")
    addWaitingBtn = QPushButton("add waiting list")
    addUnavailableBtn = QPushButton("add unavailable list")
    addUnavailableBtn.setEnabled(False)
    addWaitingBtn.setEnabled(False)
    
    saveBtn = QPushButton("save")
    loadBtn = QPushButton("load")
    
    layout = QVBoxLayout()
    layout.addWidget(addClubBtn)
    layout.addWidget(addPlayerBtn)
    layout.addSpacing(1)
    layout.addWidget(addCourtBtn)
    layout.addWidget(addUpNextBtn)
    layout.addWidget(addWaitingBtn)
    layout.addWidget(addUnavailableBtn)
    layout.addSpacing(1)
    layout.addWidget(saveBtn)
    layout.addWidget(loadBtn)

    self.setLayout(layout)

    addClubBtn.clicked.connect(self.addClub)
    addPlayerBtn.clicked.connect(self.addPlayer)

    addCourtBtn.clicked.connect(self.addCourt)
    addUpNextBtn.clicked.connect(self.addUpNext)
    addWaitingBtn.clicked.connect(self.addWaiting)
    addUnavailableBtn.clicked.connect(self.addUnavailable)

    saveBtn.clicked.connect(self.save)
    loadBtn.clicked.connect(self.load)

  def addClub(self):
    self._addUnary(self.addClubSignal, "Club Name")

  def addPlayer(self):
    dialog = NewPlayerDialog()
    if (dialog.exec_() and dialog.playerName):
      self.addPlayerSignal.emit(dialog.playerName, dialog.gender)

  def addCourt(self):
    self._addUnary(self.addCourtSignal)
  
  def addUpNext(self):
    self._addUnary(self.addUpNextSignal)
  
  def addWaiting(self):
    self._addUnary(self.addWaitingSignal, "Waiting list name")
  
  def addUnavailable(self):
    self._addUnary(self.addUnavailableSignal)

  def save(self):
    filename = QFileDialog.getSaveFileName(None, "Choose save file")
    if filename:
      self.saveSignal.emit(str(filename))
  
  def load(self):
    filename = QFileDialog.getOpenFileName(None, "Choose session file")
    if filename:
      self.loadSignal.emit(filename)

  def _addUnary(self, signal, labelStr = None):
    execInputDialogAndEmit(signal, labelStr)

class NewPlayerDialog(QDialog):
  def __init__(self, *args):
    super(NewPlayerDialog, self).__init__(*args)

    hlayout = QHBoxLayout()
    vlayout = QVBoxLayout()

    self._lineEdit = QLineEdit()

    hlayout.addWidget(QLabel("Player Name:"))
    
    hlayout.addWidget(self._lineEdit, 1)

    vlayout.addLayout(hlayout)
    
    self._male = QRadioButton("Male")
    self._female = QRadioButton("Female")
    vlayout.addWidget(self._male)
    vlayout.addWidget(self._female)

    hlayout2 = QHBoxLayout()
    ok = QPushButton("done")
    cancel = QPushButton("cancel")
    hlayout2.addWidget(ok)
    hlayout2.addWidget(cancel)

    vlayout.addLayout(hlayout2)

    self.setLayout(vlayout)

    self._male.setChecked(True)

    ok.clicked.connect(self.save)
    cancel.clicked.connect(self.reject)

    self.playerName = ""
    self.gender = ""

  def save(self):
    self.playerName = str(self._lineEdit.text())
    self.gender = "m" if self._male.isChecked() else "f"
    self.accept()

