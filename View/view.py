import sys
from PySide import QtGui, QtCore

from config import Config

from item import Item
from itemqueue import ItemQueue
from itemmanager import ItemManager
from operations import execInputDialog

class MyView(QtGui.QGraphicsView):
  moveQueueContents = QtCore.Signal(str, str)
  movePlayers = QtCore.Signal(list, str, str)
  reorderQueue = QtCore.Signal(str, list)

  def __init__(self, session, controller):
    super(MyView, self).__init__()

    self._session = session
    self._controller = controller

    self.setDragMode(self.RubberBandDrag)
    self.setRubberBandSelectionMode(QtCore.Qt.IntersectsItemBoundingRect)

    self.setGeometry(QtCore.QRect(40, 40, 650, 450))

    self.setScene(QtGui.QGraphicsScene(self))

    gradient = QtGui.QLinearGradient(0, 0, 0, 300);
    gradient.setColorAt(0, QtCore.Qt.gray);
    gradient.setColorAt(1, QtCore.Qt.white);
    self.scene().setBackgroundBrush(gradient)

    self.itemMgr = ItemManager(session.Queries)
    
    self.itemMgr.moveQueueContents.connect(self.moveQueueContents)
    self.itemMgr.movePlayers.connect(self.movePlayers)
    self.itemMgr.reorderQueue.connect(self.reorderQueue)

    self._connectToSession(self._session, self.itemMgr)
    
    self.moveQueueContents.connect(controller.moveQueueContents)
    self.movePlayers.connect(controller.moveMulti)
    self.reorderQueue.connect(controller.reorder)
  
  def clearAll(self):
    self.itemMgr.reset()
    self.scene().clear()

  def addUnavailable(self, name):
    unavailable = ItemQueue(Item.Size, -1, name)
    unavailable.setPos(450, 50)
    self.scene().addItem(unavailable)

    self.itemMgr._addQueue(unavailable)

  def addItem(self, name, gender):
    color = Config.MaleColor if gender == 'm' else Config.FemaleColor
    player = Item(self.itemMgr, name, color)
    
    self.scene().addItem(player)
    self.itemMgr.addItem(player)

  def addItemToQ(self, name, qName):
    self.itemMgr.addItemToQueue(name, qName)
  
  def removeItemFromQ(self, name, qName):
    self.itemMgr.removeItemFromQueue(name, qName)

  def addCourt(self, name):
    crt = ItemQueue(Item.Size, 4, name)
    crt.setPos(50, 50)
    self.scene().addItem(crt)
    self.itemMgr.addCourt(crt)

  def addUpNext(self, name):
    crt = ItemQueue(Item.Size, 4, name)
    crt.setPos(50, 50)
    self.scene().addItem(crt)
    self.itemMgr.addUpNext(crt)

  def addWaiting(self, name):
    crt = ItemQueue(Item.Size, -1, name)
    crt.setPos(50, 50)
    self.scene().addItem(crt)
    self.itemMgr.setWaiting(crt)

  def contextMenuEvent(self, event):
    item = self.itemAt(event.pos())
    
    if not self.itemMgr.graphicsItemIsRenamable(item):
      return

    name = item.name

    menu = QtGui.QMenu()
    action = menu.addAction("rename")
    
    def requestRename():
      newName = execInputDialog("Enter New Name")
      if newName:
        self._controller.renameQueue(name, newName)
    
    action.triggered.connect(requestRename)
    menu.move(event.globalPos())
    menu.exec_()

  def _connectToSession(self, session, itemMgr):
    session.dataReset.connect(self.clearAll)

    session.playerCreated.connect(self.addItem)
    
    session.playerAdded.connect(self.addItemToQ)
    session.playerRemoved.connect(self.removeItemFromQ)

    session.courtAdded.connect(self.addCourt)
    session.upNextAdded.connect(self.addUpNext)
    session.waitingAdded.connect(self.addWaiting)
    session.unavailableAdded.connect(self.addUnavailable)
    session.queueRenamed.connect(itemMgr.renameQueue)

    session.operationRejected.connect(self._operationRejected)

  def _detachFromSession(self):
    raise NotImplementedError

  def _operationRejected(self, msg):
    print 'rejected:{0}'.format(msg)
    
    QtGui.QMessageBox.warning(None, "Operation failed.", msg)
    self.itemMgr.forceRefresh();
  
##############################################################

