from PySide.QtCore import QObject

from Model.session import Session, BatchOperation
from Model.court import Court

def ReportErrors(func):
  def safeCall(*args, **kwds):
    try:
      func(*args, **kwds)
    except Exception as ex:
      import traceback
      traceback.print_exc()
  return safeCall

class Controller(QObject):
  def __init__(self, session):
    super(Controller, self).__init__()

    self.session = session
  
  ################# save / load ###############################################

  def saveSession(self, filename):
    self.session.save(filename)


  def loadSession(self, filename):
    with BatchOperation(self.session):
      self.session.reset();
      self.session.load(filename);
    
  def resetSession(self):
    with BatchOperation(self.session):
      self.session.reset();

  ################### static stuff

  def addClub(self, name):
    with BatchOperation(self.session):
      self.session.data.addClub(name)
      #self.session.data.addUnavailable("Unavailable")

  def removeClub(self, name):
    with BatchOperation(self.session):
      self.session.data.removeClub(name)

  def addUnavailableList(self, name):
    with BatchOperation(self.session):
      self.session.data.addUnavailable(name)

  def removeUnavailableList(self, name):
    with BatchOperation(self.session):
      self.session.data.removeUnavailable(name)

  def addWaitingList(self, name):
    with BatchOperation(self.session):
      self.session.data.addWaiting(name)

  def removeWaitingList(self, name):
    with BatchOperation(self.session):
      self.session.data.removeWaiting(name)

  def addUpNext(self, name):
    with BatchOperation(self.session):
      self.session.addUpNext(name)

  def removeUpNext(self, name):
    with BatchOperation(self.session):
      self.session.data.removeUpNext(name)

  def addCourt(self, name):
    with BatchOperation(self.session):
      self.session.addCourt(name)

  def removeCourt(self, name):
    with BatchOperation(self.session):
      self.session.data.removeCourt(name)

  def renameQueue(self, current, new):
    with BatchOperation(self.session):
      self.session.renameQueue(current, new)

  ###################  club membership  #######################################
  
  @ReportErrors
  def addToClub(self, name, gender):
    with BatchOperation(self.session):
      member = self.session.data.registerPlayer(name, gender)

  def removeFromClub(self, name):
    self.session.data._d.club.addMember(name, gender)

  #############################################################################

  def __move(self, playerName, fromQname, toQname):
    with BatchOperation(self.session):
      self.session.move(playerName, fromQname, toQname)

  def moveMulti(self, playerNames, fromQname, toQname):
    with BatchOperation(self.session):
      self.session.moveMulti(playerNames, fromQname, toQname)

  def reorder(self, qName, newNameOrder):
    q1 = self.findQueue(qName);

    with BatchOperation(self.session):
      self.session.data.reorderQueue(qName, newNameOrder)

  def moveQueueContents(self, fromQname, toQname):
    with BatchOperation(self.session):
      self.session.data.moveQueueContents(fromQname, toQname)

  def findQueue(self, name):
    return self.session.data._d.findQueue(name)


  #def addToSession(self, name):
  #  self.session.data._club.setAvailable(name)
  #  self.session.data._waiting.append(self.session.data._club.getMember(name))

  #def removeFromSession(self, name):
  #  self.session.data._club.setUnavailable(name)
  #  self.session.data._waiting.remove(self.session.data._club.getMember(name))

  #def addToCourt(self, playerName, courtName):
  #  self.session.data._courts[courtName].Players.append(self.session.data._club.getMember(name))

  #def removeFromCourt(self, playerName, courtName):
  #  self.session.data._courts[courtName].Players.remove(self.session.data._club.getMember(name))

  #def addToUpNext(self, playerName, upnextName):
  #  self.session.data._upNexts[upnextName].Players.append(self.session.data._club.getMember(name))

  #def removeFromUpNext(self, playerName, upnextName):
  #  self.session.data._upNexts[upnextName].Players.remove(self.session.data._club.getMember(name))

  #def addToWaiting(self, playerName):
  #  self.session.data._waiting.append(self.session.data._club.getMember(name))

  #def removeFromWaiting(self, playerName):
  #  self.session.data._waiting.remove(self.session.data._club.getMember(name))