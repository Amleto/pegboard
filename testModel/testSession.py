from PySide.QtCore import Signal

from nose.tools import *

from Model.session import Session, BatchOperation
from Model.data import DataSignals

def testCanInstantiateSession():
  try:
    session = Session()
  except:
    ok_(False, "failed to instantiate Session")

class testSession:

  class Observer(object):
    def __init__(self, session):
      self.operations = []
      self._session = session

      self.slots = []

      for attr in DataSignals.__dict__:
        if isinstance(DataSignals.__dict__[attr], Signal):
          print "signal found in DataSignals*: {0}\n  *(assuming it is also present on Session)".format(attr)
          slot = self.makeSlot(attr)
          
          self.slots.append(slot)
          
          session.__getattribute__(attr).connect(slot)

    def makeSlot(self, signalName):
      print 'making slot {0}'.format(signalName)
      def slot(*args):
        print 'in Observer slot {0}'.format(signalName)
        stuff = [signalName]
        stuff.extend(args)
        self.operations.append(stuff)

      return slot

  #def testSessionEmitsNoSignalsIfStartUpdateNotCalled(self):
  #  session = Session()
  #  obs = testSession.Observer(session)
      
  #  session.data.registerPlayer("player1", "m")
  #  session.data.addCourt("crt1")
  #  session.data.addPlayer("player1", "crt1")

  #  assert len(obs.operations) == 0

  def __init__(self):
    self.session = None

  def setup(self):
    print 'setup'
    self.session = Session()

  def teardown(self):
    print 'teardown'
    self.session = None

  @staticmethod
  def simpleClubSetup(session):
    with BatchOperation(session):
      session.data.addClub("the club")
      session.data.registerPlayer("player1", "m")
      session.data.registerPlayer("player2", "m")
      session.data.registerPlayer("player3", "m")
      session.data.registerPlayer("player4", "m")
      session.data.addPlayer("player1", "Waiting")
      session.data.addPlayer("player2", "Waiting")
      session.data.addPlayer("player3", "Waiting")
      session.data.addPlayer("player4", "Waiting")

  def testSessionIsConnectedToData(self):
    session = self.session
    obs = testSession.Observer(session)

    with BatchOperation(session):
      session.data.addClub("the club")

    assert len(obs.operations)

  def testAddClub(self):
    session = self.session
    obs = testSession.Observer(session)

    with BatchOperation(session):
      session.data.addClub("the club")

    assert session.data._d.club
    assert session.data._d.club.Name == "the club"
    assert obs.operations[0]
    assert obs.operations[0][0] == "clubAdded"

  def testRemoveClub(self):
    session = self.session
    obs = testSession.Observer(session)

    with BatchOperation(session):
      session.data.addClub("the club")

    with BatchOperation(session):
      session.data.removeClub("the club")

    assert session.data._d.club is None
    
  @raises(ValueError)
  def testCantAddTwoClubs(self):
    session = self.session
    obs = testSession.Observer(session)

    with BatchOperation(session):
      session.data.addClub("the club")
    
    with BatchOperation(session):
      session.data.addClub("the club")

  def testRegisterPlayerAddsPlayerToTheClub(self):
    session = self.session
    
    with BatchOperation(session):
      session.data.addClub("the club")
      session.data.registerPlayer("player", "m")

    assert session.data._d.club.getMember("player")

  def testRegisterPlayerEmitsPlayerCreated(self):
    session = self.session
    
    with BatchOperation(session):
      session.data.addClub("the club")
    
    obs = testSession.Observer(session)
    
    with BatchOperation(session):
      session.data.registerPlayer("player", "m")

    assert obs.operations[0][0] == "playerCreated"

  def testAddPlayer(self):
    session = self.session
    
    with BatchOperation(session):
      session.data.addClub("the club")
      session.data.registerPlayer("player1", "m")

    with BatchOperation(session):
      session.data.addPlayer("player1", "Waiting")

    assert session.data._d.club.getMember("player1") in session.data._d.findQueue("Waiting").items()

  @raises(ValueError)
  def testRemovePlayerWhenNotInQueueRaisesValueError(self):
    session = self.session
    with BatchOperation(session):
      session.data.addClub("the club")
      session.data.registerPlayer("player1", "m")

    with BatchOperation(session):
      session.data.removePlayer("player1", "Waiting")

  def testWhenAddThenRemovePlayerThenPlayerNotInQueue(self):
    session = self.session
    with BatchOperation(session):
      session.data.addClub("the club")
      session.data.registerPlayer("player1", "m")

    with BatchOperation(session):
      session.data.addPlayer("player1", "Waiting")
    with BatchOperation(session):
      session.data.removePlayer("player1", "Waiting")
    
    member = session.data._d.club.getMember("player1")
    que = session.data._d.findQueue("Waiting")
    assert member not in que.items()

    with BatchOperation(session):
      session.data.addPlayer("player1", "Waiting")
      session.data.removePlayer("player1", "Waiting")

    assert member not in que.items()

  def testReorderQueue(self):
    import itertools
    names = ["player1", "player2", "player3", "player4"]
    permutations = itertools.permutations(names)
    for permutation in permutations:
      yield self._doReorderQueue, "Waiting", permutation

  def _doReorderQueue(self, qName, playerNames):
    session = self.session
    self.simpleClubSetup(session)
    session.data.reorderQueue(qName, playerNames)

    for mbr, expectedName in zip (session.data._d.findQueue(qName).items(), playerNames):
      assert mbr.Name == expectedName

  def testRenameQueueEmitsQueueRenamed(self):
    session = self.session
    
    with BatchOperation(session):
      session.data.addClub("the club")
      session.addCourt("crt1")

    obs = testSession.Observer(session)
    
    with BatchOperation(session):
      session.renameQueue("crt1", "court two")

    assert obs.operations[0][0] == "queueRenamed"
