import Model

from nose.tools import ok_

def testSaveLoad():
  data = Model.data.Data()
  
  data.addClub("the club")
  data.addCourt("crt1")
  data.addUpNext("upnext1")

  data.registerPlayer("man1", "m")
  data.registerPlayer("lady1", "f")

  filename = "testSaveLoad.pgb"

  data.save(filename)

  data.reset()

  data.load(filename)

  try:
    ok_ (data._d.findQueue("Waiting") is not None)
    ok_ (data._d.findQueue("crt1") is not None)
    ok_ (data._d.findQueue("upnext1") is not None)
    ok_ (data._d.findQueue("Unavailable") is not None)

    ok_ (data._d.club.Name == "the club")
  
  finally:
    import os
    os.remove(filename)