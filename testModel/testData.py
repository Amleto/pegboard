from PySide.QtCore import QObject
from PySide.QtCore import Signal

from Model import find

from Model.data import Data, DataSignals

from nose.tools import ok_, raises

class DataSignalsSubClass(DataSignals):
  pass

class Target(Data):
  def __init__(self):
    super(Target, self).__init__()
    for sig, name in self._signalAndNames():
      sig.connect(self._makeSlot(name))

    self.slotcalls = []

  def _makeSlot(self, name):
    def slot(*args):
      print 'in slot {0}'.format(name)
      calldetails = [name]
      calldetails.extend(args)
      self.slotcalls.append(calldetails)
      return name, args
    return slot

class TestDataSignals(object):

  def testDataSignalsIsIterable(self):
    hasErr = False
    ex = None
    try:
      ds = DataSignals()
      for signal in ds:
        pass
    except Exception, e:
      ex = e
      hasErr = True

    ok_(not hasErr, str(ex))

  def testDataSignalsIterItemIsPyqtBoundSignal(self):
    ds = DataSignals()
    
    sigCount = 0
    for signal in ds:
      sigCount += 1
      ok_(isinstance(signal, Signal), "iter item {0} is not a {1})".format(signal, Signal))
    ok_(sigCount, "expected more than zero signals")

  def testDataSignalsSubClassIsIterable(self):
    hasErr = False
    ex = None
    try:
      ds = DataSignalsSubClass()
      for signal in ds:
        pass
    except Exception, e:
      ex = e
      hasErr = True

    ok_(not hasErr, str(ex))

  def testDataSignalsSubClassIterItemIsPyqtBoundSignal(self):
    ds = DataSignalsSubClass()
    
    sigCount = 0
    for signal in ds:
      sigCount += 1
      print type(signal)
      ok_(isinstance(signal, Signal), "iter item {0} is not a {1})".format(signal, Signal))
    ok_(sigCount, "expected more than zero signals")

  def testDataSignalsAutoConnection(self):
    ds = DataSignals()
    target = Target()
    ds.autoConnect(target)
    
    ds.clubAdded.emit("the club")           #0
    ds.clubRemoved.emit("the club")         #1
    ds.playerAdded.emit("player1", "m")     #2
    ds.courtAdded.emit("Court1")            #3
    ds.unavailableRemoved.emit("unav")      #4

    ok_(target.slotcalls[0][0] == "clubAdded")
    ok_(target.slotcalls[0][1] == "the club")

    ok_(target.slotcalls[1][0] == "clubRemoved")
    ok_(target.slotcalls[1][1] == "the club")

    ok_(target.slotcalls[2][0] == "playerAdded")
    ok_(target.slotcalls[2][1] == "player1")
    ok_(target.slotcalls[2][2] == "m")

    ok_(target.slotcalls[3][0] == "courtAdded")
    ok_(target.slotcalls[3][1] == "Court1")

    ok_(target.slotcalls[4][0] == "unavailableRemoved")
    ok_(target.slotcalls[4][1] == "unav")

class TestData(object):

  def setup(self):
    self.data = Data()
  
  def testWhenPlayerIsRegisteredThenPlayerIsAddedToUnavailableList(self):
    self.data.addClub("club")
    self.data.registerPlayer("p1", "m")

    mbr = self.data._d.club.getMember("p1")

    ok_ (mbr in self.data._d.unavailable)

  def testWhenClubIsAddedThenUnavailableIsCreated(self):
    self.data.addClub("club")

    ok_ (self.data._d.unavailable is not None)

  def testWhenClubIsAddedThenWaitingIsCreated(self):
    self.data.addClub("club")

    ok_ (self.data._d.waiting is not None)

  def _renameQueue(self, addQueueFunc, name, newname):
    self.data.addClub("club")
    addQueueFunc(name)
    self.data.renameQueue(name, newname)

    ok_ (self.data._d.findQueue(newname) is not None)
    ok_ (self.data._d.findQueue(name) is None)

  def testRenameCourt(self):
    self._renameQueue(self.data.addCourt, "court 1", "COURT_ONE")
  def testRenameUpNext(self):
    self._renameQueue(self.data.addUpNext, "court 1", "COURT_ONE")
  def testRenameWaiting(self):
    self._renameQueue(lambda w : None, "Waiting", "waiting_list")
  def testRenameUnavailable(self):
    self._renameQueue(lambda u : None, "Unavailable", "At Home")

  @raises(ValueError)
  def testExceptionRaisedWhenRenamingClash(self):
    self.data.addClub("club")
    self.data.addCourt("court one")
    self.data.addCourt("court two")

    self.data.renameQueue("court one", "court two")

  @raises(ValueError)
  def testExceptionRaisedWhenRenamingClashFromDifferentContainerType(self):
    self.data.addClub("club")
    self.data.addCourt("court one")
    self.data.addUpNext("court two")

    self.data.renameQueue("court one", "court two")

  @raises(ValueError)
  def testExceptionRaisedWhenRenamingCourtThatDoesNotExist(self):
    self.data.addClub("club")
    self.data.addCourt("court one")
    self.data.addCourt("court two")

    self.data.renameQueue("court nowhere", "court umpteen")
