
from Model.simpleQueue import queue

from nose.tools import ok_

def testQueue_inKeyword():
  q = queue()
  q.pushbackMulti((1,2,3,'a','abc'))

  ok_ (1 in q)
  ok_ (2 in q)
  ok_ (3 in q)
  ok_ (4 not in q)

  ok_ ('a' in q)
  ok_ ('abc' in q)
  
  ok_ ('b' not in q)
  ok_ ('abcd' not in q)


def testQueue_iter():
  que = queue()
  
  items = (1,2,3,'a','abc')
  que.pushbackMulti(items)

  for (i,q) in zip(items, list(que)):
    print i,q
    ok_ (i == q)

def testQueue_lenKeyword():
  q = queue()
  q.pushbackMulti((1,2,3,'a','abc'))

  ok_ (len(q) == 5)