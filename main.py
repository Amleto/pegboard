import sys
from PySide import QtGui, QtCore

from config import Config

from View.view import MyView
from View.operations import ControlWidget

from Controller.controller import Controller
from Model.session import Session

if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    
    ops = ControlWidget()

    session = Session()
    controller = Controller(session)
    view = MyView(session, controller)

    splitter = QtGui.QSplitter(QtCore.Qt.Horizontal)
    splitter.addWidget(ops)
    splitter.addWidget(view)
    
    ops.addClubSignal.connect(controller.addClub)
    ops.addPlayerSignal.connect(controller.addToClub)
    
    ops.addCourtSignal.connect(controller.addCourt)
    ops.addUpNextSignal.connect(controller.addUpNext)
    ops.addWaitingSignal.connect(controller.addWaitingList)
    
    #ops.addUnavailableSignal.connect(controller.addUnavailableList)

    ops.saveSignal.connect(controller.saveSession)
    ops.loadSignal.connect(controller.loadSession)

    
    splitter.show()
    splitter.resize(600,400)

    
    
    app.exec_()
    
    
