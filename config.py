from PySide.QtGui import QColor
from PySide.QtCore import Qt

class Config:
  ItemWidth = 60
  ItemHeight = 20
  Spacing = 5
  
  MaleColor = QColor(Qt.cyan).lighter(125);
  FemaleColor = QColor(Qt.red).lighter(150)
